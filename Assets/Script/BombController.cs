﻿using UnityEngine;
using System.Collections;

// Applies an explosion force to all nearby rigidbodies
public class BombController : MonoBehaviour
{
    public static BombController instance;
    public float radius = 15.0F;
    public float power = 10.0F;
    public float upForce;

    public void Awake()
    {
        instance = this;
    }
    public void BombStart()
    {

        Vector3 explosionPos = transform.position;
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius);
        foreach (Collider hit in colliders)
        {
            Rigidbody rb = this.GetComponent<Rigidbody>();

            if (rb != null)
                rb.AddExplosionForce(power, hit.transform.position, radius, upForce ,ForceMode.Impulse);
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Water")
        {
            gameObject.SetActive(false);

            CounterManager.instance.CounterController();

        }
    }
        public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag=="PoduimObject" )
        {
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
        }
        if (collision.gameObject.tag == "Bomb")
        {
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            BombStart();

        }
        if (collision.gameObject.tag == "Man")
        {
            gameObject.GetComponent<Rigidbody>().isKinematic = false;
            BombStart();
        }

    }
}