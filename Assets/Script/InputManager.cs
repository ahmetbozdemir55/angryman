﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    [SerializeField] private Transform _targetPointer = default;
    [SerializeField] private Transform _firePoint = default;
    [SerializeField] private GameObject _bomb = default;

    public GameManager gameManager;
    public SlingMove slingMove;
    public GameStatus gameStatus;

    private float _bombDrag;

    public bool isstart;
    public CharacterController character;



    private Vector2 delta = Vector3.zero;
    private Vector2 lastPos = Vector3.zero;

    public float num1;
    public float num2;

    private bool _aiming = false;
    public bool isShootable;

    private void Start()
    {
        isShootable = false;

        _targetPointer.position = new Vector3(_firePoint.position.x, 3, _firePoint.position.z);
    }

    private void Update()
    {
        if (gameManager.gameStatus != GameStatus.start && isShootable)
        {
            return;
        }

        if (Input.GetMouseButtonDown(0))
        {
            _aiming = true;
            lastPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            _targetPointer.position = new Vector3(_firePoint.position.x, 3, _firePoint.position.z);
            _targetPointer.gameObject.SetActive(true);


        }
        if (Input.GetMouseButton(0))
        {
            delta = new Vector2(Input.mousePosition.x, Input.mousePosition.y) - lastPos;
            lastPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);

            num1 = Input.GetAxisRaw("Horizontal");
            num2 = Input.GetAxisRaw("Vertical");

            num1 += delta.x;
            num2 += delta.y;
            slingMove.SlingMoveGetMouseButton();


            _targetPointer.Translate(new Vector3(num1, 0f, num2));


            //MAX NOKTA BELİRLEEK İÇİN 
            //var direction = _targetPointer.position - _firePoint.position;
            //Debug.Log(direction.magnitude);
            //if (_currentLevel.maxThrowDistance < direction.magnitude)
            //{

            //    _targetPointer.position = _firePoint.position + direction.normalized * _currentLevel.maxThrowDistance;
            //}


            DrawPath();

        }
        if (Input.GetMouseButtonUp(0))
        {
            slingMove.SlingMoveGetMouseButtonUp();
 
            ClearPath();

            _targetPointer.gameObject.SetActive(false);

            character.transform.SetParent(null);
            character.GetComponent<Rigidbody>().isKinematic = false;
           character.GetComponent<Rigidbody>().velocity = CalcBallisticVelocityVector(_firePoint.position, _targetPointer.position, 30);

            gameManager.CreatCharacter();

            num1 = 0;
            num2 = 0;

        }
    }
    private void DrawPath()
    {
        ClearPath();
        Vector3[] points = CalculatePath();
        for (int i = 0; i < points.Length; i++)
        {
            var dot = ObjectPooler.instance.GetObject("dot");
            dot.transform.position = points[i];
            dot.SetActive(true);
        }


    }
    private void ClearPath()
    {
        ObjectPooler.instance.ClearObjects("dot");
    }
    private Vector3[] CalculatePath()
    {
        Vector3[] points = new Vector3[30];
        Vector3 vel = CalcBallisticVelocityVector(_firePoint.position, _targetPointer.position, 5);
        for (int i = 0; i < 30; i++)
        {
            float t = i / 10f;
            points[i] = _firePoint.position + vel * t + 0.5f * Physics.gravity * t * t;
            if (points[i].y <= 0f)
            {
                Array.Resize(ref points, i);
                break;
            }

            RaycastHit hit;
            //if (i > 0 && Physics.SphereCast(new Ray(points[i-1], points[i] - points[i- 1]), 0.2f, out hit, Vector3.Distance(points[i-1], points[i]), ~0, QueryTriggerInteraction.Ignore))
            if (i > 0 && Physics.Linecast(points[i - 1], points[i], out hit, ~0, QueryTriggerInteraction.Ignore))
            {
                Array.Resize(ref points, i);
                break;
            }
            //  vel -= _bombDrag * t * vel;

        }
        return points;
    }
    private Vector3 CalcBallisticVelocityVector(Vector3 source, Vector3 target, float angle)
    {

        Vector3 direction = target - source;
        var h = direction.magnitude * Mathf.Tan(angle * Mathf.Deg2Rad);
        h = h > 0 ? Mathf.Sqrt(h) : -Mathf.Sqrt(-h);
        float displacementY = target.y - source.y;
        Vector3 displacementXZ = new Vector3(target.x - source.x, 0, target.z - source.z);
        var time = h > 0 ? Mathf.Sqrt(-2 * h / Physics.gravity.y) : -Mathf.Sqrt(2 * h / Physics.gravity.y);
        time += (displacementY - h) > 0 ? Mathf.Sqrt(-2 * (displacementY - h) / Physics.gravity.y) : Mathf.Sqrt(2 * (displacementY - h) / Physics.gravity.y);
        Vector3 velocityY = Vector3.up * (h > 0 ? Mathf.Sqrt(-2 * Physics.gravity.y * h) : Mathf.Sqrt(+2 * Physics.gravity.y * h));
        Vector3 velocityXZ = displacementXZ / time;
        return velocityXZ + velocityY * -Mathf.Sign(Physics.gravity.y);
    }


}