﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombListController : MonoBehaviour
{
    public GameObject Cube;
    public Rigidbody[] BombList;

    // Start is called before the first frame update
    void Start()
    {
        BombList = GetComponentsInChildren<Rigidbody>(true);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
