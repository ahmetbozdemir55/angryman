﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    public CounterManager counterManager;

    public float thrust = 5.0f;
    public Rigidbody rb; 

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Water")
        {
            gameObject.SetActive(false);

            CounterManager.instance.CounterController();
            //counterManager.CounterController();

        }
        if (other.gameObject.tag == "Man")
        {
            rb = GetComponent<Rigidbody>();
            rb.AddForce(0, 0, thrust, ForceMode.Impulse);
        }
    }
}
