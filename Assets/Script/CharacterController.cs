﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CharacterController : MonoBehaviour
{
    public EffectManager effectManager;

    public GameObject Cube;
    public GameObject Band;

    public GameObject ObstacleObje;
    public InputManager inputManager;
    public int ObstacleCalculate;

    // Start is called before the first frame update

    void Start()
    {
        Cube = GameObject.FindWithTag("Bombbase");
        ObstacleObje = GameObject.FindWithTag("ObstacleObjeBase");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void ClaimAnim()
    {
        inputManager.isShootable = false;
        transform.DOMove(new Vector3(3.6f, 9.4f, -39.2f), 2f).OnComplete(ChangeShootable) ;
    }

    public void ChangeShootable()
    {
        transform.SetParent(Band.transform);
        this.GetComponent<BoxCollider>().enabled = true;
        inputManager.isShootable = true;

    }
    public void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag=="Water")
        {
            gameObject.SetActive(false);

        }

    }
    public void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bomb")
        {
            EffectManager.instance.BombEffcet();
            AllBombisKinematicFalse();
            BombController.instance.BombStart();
        }
        if (collision.gameObject.tag == "PoduimObject")
        {
            EffectManager.instance.ContactEffcet();
            AllObstacleKinematicFalse();

        }
    }
    public void AllObstacleKinematicFalse()
    {

        Rigidbody[] isKinematicistObstacle = ObstacleObje.transform.GetComponentsInChildren<Rigidbody>();
        ObstacleCalculate = isKinematicistObstacle.Length;

        foreach (var item in isKinematicistObstacle)
        {
            
            item.isKinematic = false;
        }
    }
    public void AllBombisKinematicFalse()
    {
        Rigidbody[] isKinematicistBomb = Cube.transform.GetComponentsInChildren<Rigidbody>();
        ObstacleCalculate += isKinematicistBomb.Length;
        foreach (var item in isKinematicistBomb)
        {
            item.isKinematic = false;
        }
    }
}
