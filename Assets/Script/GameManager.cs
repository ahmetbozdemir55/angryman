﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum GameStatus
{
    menü,
    start,
    finish,
    exit
}

public class GameManager : MonoBehaviour
{
    public int Level;
    //public fireman ballController;
    public GameStatus gameStatus;
    public InputManager inputManager;
    public List<GameObject> Levels;
    public int customLevel;
    public CharacterController character;
    public GameObject Band;
    void Awake()
    {
       Level = PlayerPrefs.GetInt("level", 1);
        //PlayerPrefs.SetInt("level", 2);
        gameStatus = GameStatus.menü;

        if (Level > 2)
        {
            customLevel = Random.Range(1, 3);


            Levels[customLevel - 1].SetActive(true);
        }
        else
        {
            customLevel = Level;
            Levels[Level - 1].SetActive(true);
        }



    }
    public void Start()
    {
        CreatCharacter();
    }

    public void CreatCharacter()
    {
       var g= Instantiate(character.gameObject, new Vector3(3.6f, -2.8f, -39.2f), Quaternion.identity);
        inputManager.character = g.GetComponent<CharacterController>();
        g.GetComponent<CharacterController>().inputManager = inputManager;
        g.GetComponent<CharacterController>().ClaimAnim();
        g.GetComponent<CharacterController>().Band = Band;
    }

    public void StartGame()
    {
        Invoke("Delay", 1f);
    }
    public void Delay()
    {
        gameStatus = GameStatus.start;

    }
    public void ClickExit()
    {
        gameStatus = GameStatus.exit;
    }
    public void GameFinish()
    {
        gameStatus = GameStatus.finish;

    }
    // Update is called once per frame
    void Update()
    {

    }

}
