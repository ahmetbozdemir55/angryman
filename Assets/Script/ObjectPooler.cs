﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    public static ObjectPooler instance;
    private Dictionary<string, List<GameObject>> pooledObjects;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
            pooledObjects = new Dictionary<string, List<GameObject>>();
        }
    }

    public GameObject GetObject(string name)
    {
        if (!pooledObjects.ContainsKey(name))
        {
            pooledObjects[name] = new List<GameObject>();
        }
        for (int i = 0; i < pooledObjects[name].Count; i++)
        {
            if (!pooledObjects[name][i].activeInHierarchy)
            {
                return pooledObjects[name][i];
            }
        }
        var instance = Instantiate(Resources.Load(name)) as GameObject;
        pooledObjects[name].Add(instance);
        return instance;
    }

    public void ClearObjects(string name)
    {
        if (!pooledObjects.ContainsKey(name))
        {
            return;
        }
        for (int i = 0; i < pooledObjects[name].Count; i++)
        {
            pooledObjects[name][i].SetActive(false);
        }
    }
}