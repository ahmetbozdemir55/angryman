﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class UIManager : MonoBehaviour
{
    public GameManager gameManager;
    public CharacterController characterController;
    public Poduim poduim;
    //public CounterManager counterManager;

    [Header("Panels")]
    public GameObject MenüPanel;
    public GameObject GamePanel;
    public GameObject FinishPanel;

    [Header("UI References")]
    [SerializeField] private Image Fill_Image;
    [SerializeField] private TextMeshProUGUI Start_Text;
    [SerializeField] private TextMeshProUGUI Finsih_Text;

    public void Awake()
    {

    }
    public void Start()
    {
       
        poduim = gameManager.Levels[gameManager.customLevel- 1].GetComponent<Poduim>();
        SetlevelText();
    }
    public void Update()
    {
        if (gameManager.gameStatus!=GameStatus.start)
        {
            return;
        }
        float newObstacleObje = CounterManager.instance.b;

        float newProgressValue = Mathf.InverseLerp(0, poduim.ObstacleCalculate, CounterManager.instance.b);
        UbdateFillmage(newProgressValue);
        if (CounterManager.instance.b==poduim.ObstacleCalculate)
        {
            GameFinish();
        }
    }
    public void GameStart()
    {
        HideAllPanel();
        GamePanel.gameObject.SetActive(true);



        gameManager.StartGame();
    }
    public void GameFinish()
    {
        HideAllPanel();
        FinishPanel.gameObject.SetActive(true);

        gameManager.GameFinish();
    }
    public void GameExit()
    {
        Application.Quit();
    }
    public void HideAllPanel()
    {
        MenüPanel.gameObject.SetActive(false);
        GamePanel.gameObject.SetActive(false);
        FinishPanel.gameObject.SetActive(false);
    }
    public void SetlevelText()
    {
        Start_Text.text = gameManager.Level.ToString();
        Finsih_Text.text = (gameManager.Level + 1).ToString();
    }
    private void UbdateFillmage(float value)
    {
        Fill_Image.fillAmount = value;
    }
    public void NextLevel()
    {
        PlayerPrefs.SetInt("level", PlayerPrefs.GetInt("level", 1)+1);
        SceneManager.LoadScene(0);
    }
}
