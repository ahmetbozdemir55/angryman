﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class EffectManager : MonoBehaviour
{
    public static EffectManager instance;
    public GameObject BombVFX;
    public GameObject ContactVFX;
    public GameObject Bomb;
    public GameObject Obstacle;
    public GameObject Confeti;

    public void Awake()
    {
        instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        Bomb = GameObject.FindWithTag("Bomb");
        Obstacle= GameObject.FindWithTag("PoduimObject");
        Confeti = GameObject.FindWithTag("Confeti");
    }
    // Update is called once per frame
    void Update()
    {
        
    }
    public void BombEffcet()
    {

        Instantiate(BombVFX,Bomb.transform.position, Bomb.transform.rotation);
        Invoke("Delay", 3f);

    }
    public void ContactEffcet()
    {
        Instantiate(ContactVFX, Obstacle.transform.position, Obstacle.transform.rotation);
        Invoke("Delay", 3f);
    }
    public void Delay()
    {
        BombVFX.gameObject.SetActive(false);
        ContactVFX.gameObject.SetActive(false);
    }
    public void FinishEffect()
    {
        //ParticleSystem[] ConfetiPlayList = Confeti.transform.GetComponentsInChildren<ParticleSystem>;
        //foreach (var item in ConfetiPlayList)
        //{

        //    item.Play();
        //}
        //PA[] isKinematicistObstacle = ObstacleObje.transform.GetComponentsInChildren<Rigidbody>();
    }
}
