﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Poduim : MonoBehaviour
{
    public GameObject Cube;
    public GameObject ObstacleObje;
    public int ObstacleCalculate;
    // Start is called before the first frame update
    void Start()
    {
        Cube = GameObject.FindWithTag("Bombbase"); 
        ObstacleObje = GameObject.FindWithTag("ObstacleObjeBase");
        Rigidbody[] ObstacleCalculateList = ObstacleObje.transform.GetComponentsInChildren<Rigidbody>();
        Rigidbody[] BombCalculateList = Cube.transform.GetComponentsInChildren<Rigidbody>();
        ObstacleCalculate = ObstacleCalculateList.Length+BombCalculateList.Length;

    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(new Vector3(0, -30, 0) * Time.deltaTime);
    }
}
